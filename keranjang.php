<?php
session_start();

echo "<pre>";
print_r($_SESSION['keranjang']);
echo "</pre>";

$koneksi = new mysqli("localhost", "kakasimwah","Kaka!@#123","Muslimah");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Keranjang Belanja</title>
    <link rel="stylesheet" href="admin/assets/css/boootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="keranjang.php">Keranjang</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.php">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="checkout.php">Checkout</a>
      </li>
      </ul>
  </div>
</nav>

<section class="konten">
    <div class="container">
    <br />
        <h1>Produk Terbaru</h1>
        <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Produk</th>
                <th>Harga</th>
                <th>Jumlah</th>
                <th>Subharga</th>
            </tr>
        </thead>
        <tbody>
            <?php $nomor=1; ?>
            <?php foreach ($_SESSION["keranjang"] as $id_produk => $jumlah); ?>
            <!-- menampilkan produk yang sedang diperulangkan berdasarkan id_produk -->
            <?php
            $ambil = $koneksi->query("SELECT * FROM produk
                    WHERE id_produk='$id_produk'");
            $pecah = $ambil->fetch_assoc();
            $subharga = $pecah["harga_produk"]*$jumlah;
        
            ?>
            <tr>
                <td><?php echo $nomor; ?></td> 
                <td><?php echo $pecah["nama_produk"]; ?></td>
                <td>Rp. <?php echo number_format($pecah["harga_produk"]); ?></td>
                <td><?php echo $jumlah; ?></td>
                <td>Rp. <?php echo number_format($subharga); ?></td>
            </tr>
            <?php $nomor++; ?>
            <?php 'endforeach' ?>
        </tbody>
    </table>

    <a href="index.php" class="btn btn-default">Lanjutkan Belanja</a>
    <a href="checkout.php" class="btn btn-primary">Checkout</a>
  </div>
</section>
</body>
</html>