<?php
session_start();
// koneksi ke database
$koneksi = new mysqli("localhost", "kakasimwah","Kaka!@#123","Muslimah");
?>

 <!DOCTYPE html>
 <html>
 <head>
    <title>toko muslimah</title>
    <link rel="stylesheet" href="admin/assets/css/boootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="keranjang.php">Keranjang</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.php">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="checkout.php">Checkout</a>
      </li>
      </ul>
  </div>
</nav>

<br />
<!-- konten -->
<section class="konten">
    <div class="container">
        <h1>Produk Terbaru</h1>

        <div class="row mx-auto">

        <?php $ambil = $koneksi->query("SELECT * FROM produk"); ?>
        <?php while($perproduk = $ambil->fetch_assoc()){ ?>
        
        <div class="card mr-3 ml-3" style="width: 13rem;">
          <img src="foto_produk/leptop.jpg" class="card-img-top" alt="...">
          <div class="card-body bg-light">
            <h3 class="card-title"><?php echo $perproduk['nama_produk']; ?></h3>
            <h5>Rp.<?php echo number_format($perproduk['harga_produk']); ?></h5>
            <a href="beli.php?id=<?php echo $perproduk['id_produk']; ?>" class="btn btn-primary">Beli</a>
          </div>
        </div>
        <?php } ?>

        </div>
    </div>
</section>

</body>
</html>
